extends TextureButton

export(Texture) var imageSound 
export(Texture) var imageNoSound

func _ready():
	if(Variables.isPlayed):
		texture_normal = imageSound
	else:
		texture_normal = imageNoSound

func _on_ButtonSound_button_down():
	Variables.isPlayed = not Variables.isPlayed
	if (Variables.isPlayed):
		texture_normal = imageSound
		AudioServer.set_bus_mute(0, false)
	else:
		texture_normal = imageNoSound
		AudioServer.set_bus_mute(0, true)

extends Label

func _ready():
	visible = false
	$Timer.connect("timeout", self, "_on_Timer_timeout")

func _on_Timer_timeout():
	visible = true

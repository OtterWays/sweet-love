extends Control

func _process(delta):
	$Label.text = str(ceil($Timer.time_left))

func _on_Timer_timeout():
	get_parent().get_tree().paused = false
	queue_free()

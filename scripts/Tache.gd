extends Area2D

export var animation = ""
signal hit

func _ready():
	$AnimatedSprite.animation = animation
	
func _on_Tache_body_entered(body):
	if body is KinematicBody2D:
		emit_signal("hit")
		queue_free()

extends Sprite

export var next_scene = ""
export var start = true
export var end = false
export var path = "res://assets/musique/phase1.wav"
var buttonSound = preload("res://scenes/ButtonSound.tscn")

func _ready():
	var button = buttonSound.instance()
	button.rect_position = Vector2(-400, -400)
	add_child(button)
	$Timer.connect("timeout", self, "_on_Timer_timeout")
	if (start):
		MusicController.play(path)

func _on_Timer_timeout():
	if (end):
		MusicController.stop()
	get_tree().change_scene("res://scenes/" + next_scene + ".tscn")

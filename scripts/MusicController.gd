extends Control

func play(path):
	stop()
	var new_track = load(path)
	$AudioStreamPlayer.stream = new_track
	$AudioStreamPlayer.play()
	
func stop():
	$AudioStreamPlayer.stop()

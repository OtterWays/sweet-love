extends Area2D

signal hit
export var animation = ""

func _ready():
	$AnimatedSprite.animation = animation

func _on_Object_body_entered(body):
	if body is KinematicBody2D:
		emit_signal("hit")
		queue_free()

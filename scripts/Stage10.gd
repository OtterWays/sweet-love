extends "res://scripts/StageWoman.gd"

func _ready():
	goal = 1
	var coeff = clamp(1 - (Variables.nb_dead / 10.0), 0.1, 1)	
	$Coeur.scale = Vector2(coeff, coeff)
	Variables.nb_dead += 1
	

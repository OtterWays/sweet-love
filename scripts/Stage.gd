extends Node2D

var buttonSound = preload("res://scenes/ButtonSound.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	var button = buttonSound.instance()
	add_child(button)
	$Object.connect("hit", $Goal, "_on_Goal_hit")

func _on_Goal_goal():
	$SFX_win.play()
	if name == "Stage5":
		$Player/AnimatedSprite.animation = "end2"
	else:
		$Player/AnimatedSprite.animation = "end"


func _on_Pikes_body_entered(body):
	if body is KinematicBody2D:
		body.get_child(0).animation = "dead"
		body.get_child(4).play()

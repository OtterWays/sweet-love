extends Area2D

signal goal

func _ready():
	$AnimatedSprite.animation = "close"

func _on_Goal_body_entered(body):
	if $AnimatedSprite.animation != "close":
		emit_signal("goal")
	
func _on_Goal_hit():
	if get_parent().name == "Stage5":
		$AnimatedSprite.animation = "empty"
		get_parent().get_child(3).get_child(3).play()
	else:
		$AnimatedSprite.animation = "open"
		get_parent().get_child(3).get_child(3).play()

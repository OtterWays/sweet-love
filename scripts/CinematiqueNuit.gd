extends Sprite

var buttonSound = preload("res://scenes/ButtonSound.tscn")

func _ready():
	var button = buttonSound.instance()
	button.imageSound = load("res://assets/iconSoundWoman.png")
	button.imageNoSound = load("res://assets/iconNoSoundWoman.png")
	button.rect_position = Vector2(-400, -400)
	add_child(button)
	visible = false
	$Timer.connect("timeout", self, "_on_Timer_timeout")

func _on_Timer_timeout():
	visible = true

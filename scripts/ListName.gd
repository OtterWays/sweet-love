extends "res://scripts/Cinematique.gd"

func _ready():
	var time = 0.1
	var duration = 1
	var children = get_children()
	for child in children:
		if child.is_class("Label"):
			child.get_child(0).wait_time = time
			child.get_child(0).one_shot = true
			child.get_child(0).start()
			time = time + duration
			duration = duration - 0.04
			if duration < 0.04:
				duration = 0.04
	$Timer.wait_time = time + 1
	$Timer.start()
 
extends Node2D

var taches = 0
export var goal = 3
var buttonSound = preload("res://scenes/ButtonSound.tscn")

func _ready():
	var button = buttonSound.instance()
	button.imageSound = load("res://assets/iconSoundWoman.png")
	button.imageNoSound = load("res://assets/iconNoSoundWoman.png")
	add_child(button)
	$Control/Label.text = "Time left: " + str($Timer.time_left).pad_decimals(2)
	get_tree().paused = true
	MusicController.play("res://assets/musique/phase1plusvite.wav")

func _process(delta):
	$Control/Label.text = "Time left: " + str($Timer.time_left).pad_decimals(2)

func _on_Timer_timeout():
	$Woman/SFX_game_over.play()
	$Woman/AnimatedSprite.animation = "dead"


func _on_Tache_hit():
	taches = taches + 1
	if taches >= goal:
		$Woman/AnimatedSprite.animation = "end"
		$Timer.paused = true


func _on_Flammes_body_entered(body):
	if body is KinematicBody2D:
		body.get_child(0).animation = "dead"
		body.get_child(3).play()

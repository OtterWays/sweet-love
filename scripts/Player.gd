extends KinematicBody2D

export (int) var run_speed = 200
export (int) var jump_speed = -600
export (int) var gravity = 1500
export var next_scene = ""

var velocity = Vector2()
var jumping = false
var end = false

func get_input():
    velocity.x = 0
    var right = Input.is_action_pressed('ui_right')
    var left = Input.is_action_pressed('ui_left')
    var jump = Input.is_action_just_pressed('ui_up')

	#deplacement
    if jump and is_on_floor():
        jumping = true
        velocity.y = jump_speed
    if right:
        velocity.x += run_speed
    if left:
        velocity.x -= run_speed
		

func _physics_process(delta):
	get_input()
	end = $AnimatedSprite.animation == "dead" or $AnimatedSprite.animation == "end" or $AnimatedSprite.animation == "end2"
	if end:
		velocity = Vector2(0, 0)
	velocity.y += gravity * delta
	if jumping and is_on_floor():
		jumping = false
	velocity = move_and_slide(velocity, Vector2(0, -1))
	
	#animation
	if end:
		if $AnimatedSprite.animation == "end2":
			$AnimatedSprite.flip_h = false
	elif velocity.x < 0:
		$AnimatedSprite.flip_h = true
	else:
		$AnimatedSprite.flip_h = false
	if end:
		pass
	elif velocity == Vector2(0, 0):
		if $AnimatedSprite.animation == "jump":
			$SFX_jump_end.play()
		$AnimatedSprite.animation = "default"
	elif not is_on_floor():
		$AnimatedSprite.animation = "jump"
	elif $AnimatedSprite.animation != "run":
		if $AnimatedSprite.animation == "jump":
			$SFX_jump_end.play()
		$AnimatedSprite.animation = "run"


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "dead":
		get_parent().get_tree().reload_current_scene()
	elif $AnimatedSprite.animation == "end" or $AnimatedSprite.animation == "end2":
		get_parent().get_tree().change_scene("res://scenes/" + next_scene + ".tscn")
